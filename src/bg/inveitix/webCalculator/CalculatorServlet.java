package bg.inveitix.webCalculator;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/CalculatorServlet")
public class CalculatorServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private double currentResult;
	private String resultLog;
	private int counter;

	
	public CalculatorServlet() {
		super();
		clearLog();
	}

	
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String requestInput = request.getParameter("link");

		if (requestInput != null) {

			if (requestInput.equals("print")) {

				// load page with formatted calculations
				request.setAttribute("resultLog", this.resultLog);
				RequestDispatcher rd = request
						.getRequestDispatcher("PrinterServlet");
				rd.forward(request, response);
			}

			if (requestInput.equals("clear")) {
				clearLog();
				printCalculator(response, 0);
			}
		}
	}

	
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String operation = request.getParameter("operation");
		double number = Double.parseDouble(request.getParameter("number"));

		this.currentResult = calculate(this.currentResult, number, operation);
		printCalculator(response, this.currentResult);
	}

	private void clearLog() {
		this.currentResult = 0;
		this.resultLog = "";
		this.counter = 0;
	}

	private double calculate(double oldResult, double num, String operation) {
		double result = oldResult;
		boolean error = false;

		switch (operation) {
		case "+":
			result += num;
			break;
		case "-":
			result -= num;
			break;
		case "*":
			result *= num;
			break;
		case "/":
			if (num == 0) {
				error = true;
			} else {
				result /= num;
			}
			break;
		default:
			error = true;
		}

		if (!error) {
			generateLogLine(oldResult, num, operation, result);
		} else {
			clearLog();
		}

		return result;
	}

	private void generateLogLine(double oldResult, double num,
			String operation, double result) {
		this.counter++;
		this.resultLog += ("" + counter + " >> " + oldResult + " " + operation
				+ " " + num + " = " + result + "<br>");
	}

	public void printCalculator(HttpServletResponse response, double result)
			throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.print("<!DOCTYPE html><html><head><meta charset=\"UTF-8\"><title>Calculator</title></head><body><div class=\"calculator\">"
				+ "<form method=\"post\" action=\"/WebCalculator/calculator\"><label>Result</label><input type=\"text\" name=\"result\" id=\"result\""
				+ "value=\""
				+ result
				+ "\" readonly><select name=\"operation\" id=\"operation\"><option value=\"+\">+</option>"
				+ "<option value=\"-\">-</option><option value=\"*\">*</option><option value=\"/\">/</option>"
				+ "</select><input type=\"text\" name=\"number\" id=\"number\">"
				+ "<button type=\"submit\">=</button></form></div><br> "
				+ "<a href=\"/WebCalculator/calculator?link=print\">Print last calculation</a>"
				+ " <a href=\"/WebCalculator/calculator?link=clear\">Clear result</a></body></html>");
	}

}
