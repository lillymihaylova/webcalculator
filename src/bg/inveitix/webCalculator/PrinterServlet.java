package bg.inveitix.webCalculator;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class PrinterServlet
 */
@WebServlet("/PrinterServlet")
public class PrinterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public PrinterServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();

		String resultLog = (String) request.getAttribute("resultLog");
		out.println("<!DOCTYPE html><html><head><meta charset=\"UTF-8\"><title>Calculator</title></head><body>");
		
		if (resultLog.equals("")) {
			out.println("<h2>No calculations</h2>");
		} else {
			out.println(resultLog);
			out.println("<br><input type=\"button\" onclick=\"javascript:print()\" value=\"Print\"</input>");
		}
		
		out.println("</body></html>");

	}

}